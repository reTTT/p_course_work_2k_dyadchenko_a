// p_2k_z05.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

#include "merge_sort.h"

struct Worker
{
	int id;
	std::string name;
	std::string position;
	double salary;

	static bool sortByID(const Worker& l, const Worker& r)
	{
		return l.id < r.id;
	}

	static bool sortByName(const Worker& l, const Worker& r)
	{
		return l.name < r.name;
	}

	static bool sortBySalary(const Worker& l, const Worker& r)
	{
		return l.salary < r.salary;
	}

	static bool sortByPosition(const Worker& l, const Worker& r)
	{
		return l.position < r.position;
	}

	static void print(const Worker& w)
	{
		std::cout << "Worker info ID: " << w.id << std::endl;
		std::cout << "Name: " << w.name << std::endl;
		std::cout << "Position: " << w.position << std::endl;
		std::cout << "Salary: " << w.salary << std::endl;
		std::cout << std::endl;
	}

	static Worker hire()
	{
		static int id = 0;

		static const std::string position[] = {"programmer", "artist", "projects manager", "producer", "musician"};
		static const std::string name[] = {"Alexey", "Oleg", "Ivan", "Denis", "Conchita Wurst"};

		Worker w;
		w.id = ++id;
		w.salary = 35000.0 + rand()%6500*10;
		w.position = position[rand()%5];
		w.name = name[rand()%5];

		return w;
	}
};

int _tmain(int argc, _TCHAR* argv[])
{
	std::vector<Worker> workers;

	for(size_t i=0; i<7; ++i)
	{
		workers.push_back(Worker::hire());
	}
	
	mergeSort(workers.begin(), workers.end()-1, Worker::sortByName);

	std::for_each(workers.begin(), workers.end(), Worker::print);
	
	return 0;
}

