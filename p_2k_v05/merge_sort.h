#pragma once

#include <iterator>
#include <vector>

template<class _It, class _Compare>
void makeMerge(_It l, _It mid, _It r, _Compare comp)
{
	typedef typename std::iterator_traits< _It >::difference_type diff_t;
	typedef typename std::iterator_traits< _It >::value_type val_t;
	
	const diff_t size = (r-l)+1;

	std::vector<val_t> tmp(size);

	_It i = l;
	_It j = mid + 1;

	for(diff_t step=0; step<size; ++step)
	{
		if((j > r) || ((i <= mid) && (comp(*i, *j))))
		{
			tmp[step] = *(i++);
		}
        else
		{
			tmp[step] = *(j++);
		}
	}

	for(diff_t step=0; step<size; ++step)
	{
		*(l+step) = tmp[step];
	}
}

template<class _It, class _Compare>
void mergeSort(_It l, _It r, _Compare comp)
{
	if(l == r) return;

	_It mid = l + (r - l)/2;

	mergeSort(l, mid, comp);
	mergeSort(mid+1, r, comp);
	makeMerge(l, mid, r, comp);
}